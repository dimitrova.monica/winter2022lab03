import java.util.Scanner;
public class Shop {
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);

        Phone[] phones = new Phone[4];

        for(int i = 0; i < 4; i++){
            phones[i] = new Phone();
            System.out.println("Enter the brand of the phone: ");
            phones[i].brand = reader.next();
            System.out.println("Enter the model of the phone: ");
            phones[i].model = reader.next();
            System.out.println("Enter the price: ");
            phones[i].price = reader.nextDouble();
        }
        System.out.println(phones[3].brand);
        System.out.println(phones[3].model);
        System.out.println(phones[3].price);

        //printing what is returned from the instance method for the last product of the array
        System.out.println(phones[3].whatIsThePrice());
    }
}
